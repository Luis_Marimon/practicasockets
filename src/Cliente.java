import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Cliente{
    Socket requestSocket;
    ObjectOutputStream out;
    ObjectInputStream in;
    String message;
    String ipConfig;
    int puerto=2004;
    Cliente(){}
    Scanner sc = new Scanner(System.in);
    void run() throws InterruptedException {
        try{
            //1. creating a socket to connect to the server
            System.out.println("Ip a elegir:");
            ipConfig = sc.nextLine();
            System.out.println("Puerto a elegir:");
            puerto = sc.nextInt();
            requestSocket = new Socket(ipConfig, puerto);
            System.out.println("Connected to: "+ipConfig+"in port: "+puerto);
            //2. get Input and Output streams
            out = new ObjectOutputStream(requestSocket.getOutputStream());
            out.flush();
            in = new ObjectInputStream(requestSocket.getInputStream());

            //3: Communicating with the server
            do{
                try{                 
                    message = sc.nextLine();                  
                    sendMessage(message);
                }
                catch(Exception e){
                    e.printStackTrace();
                }
            }while(!message.equals("bye"));
        }
        catch(UnknownHostException unknownHost){
            System.err.println("You are trying to connect to an unknown host!");
        }
        catch(IOException ioException){
            ioException.printStackTrace();
        }
        finally{
            //4: Closing connection
            try{
                in.close();
                out.close();
                requestSocket.close();
            }
            catch(IOException ioException){
                ioException.printStackTrace();
            }
        }
    }
    void sendMessage(String msg)
    {
        try{
            out.writeObject(msg);
            out.flush();
            System.out.println("client>" + msg);
        }
        catch(IOException ioException){
            ioException.printStackTrace();
        }
    }
    public static void main(String args[])
    {
        Cliente cliente = new Cliente();
        try {
            cliente.run();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
